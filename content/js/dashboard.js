/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.1306572215663125, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.6059322033898306, 500, 1500, "getStaffHealthRecords"], "isController": false}, {"data": [0.011363636363636364, 500, 1500, "getCentreHolidaysOfYear"], "isController": false}, {"data": [0.0, 500, 1500, "getPastChildrenData"], "isController": false}, {"data": [0.03875968992248062, 500, 1500, "suggestProgram"], "isController": false}, {"data": [0.34513274336283184, 500, 1500, "findAllWithdrawalDraft"], "isController": false}, {"data": [0.06048387096774194, 500, 1500, "findAllLevels"], "isController": false}, {"data": [0.022321428571428572, 500, 1500, "findAllClass"], "isController": false}, {"data": [0.2894736842105263, 500, 1500, "getStaffCheckInOutRecords"], "isController": false}, {"data": [0.09375, 500, 1500, "getChildrenToAssignToClass"], "isController": false}, {"data": [0.32916666666666666, 500, 1500, "getCountStaffCheckInOut"], "isController": false}, {"data": [0.0, 500, 1500, "searchBroadcastingScope"], "isController": false}, {"data": [0.3025210084033613, 500, 1500, "getTransferDrafts"], "isController": false}, {"data": [0.014705882352941176, 500, 1500, "registrationByID"], "isController": false}, {"data": [0.0, 500, 1500, "leadByID"], "isController": false}, {"data": [0.027131782945736434, 500, 1500, "getRegEnrolmentForm"], "isController": false}, {"data": [0.0, 500, 1500, "findAllLeads"], "isController": false}, {"data": [0.0, 500, 1500, "getEnrollmentPlansByYear"], "isController": false}, {"data": [0.025735294117647058, 500, 1500, "getAvailableVacancy"], "isController": false}, {"data": [0.0, 500, 1500, "getRegistrations"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 2541, 0, 0.0, 78846.83156237697, 3, 409523, 47158.0, 199061.60000000003, 273462.30000000005, 401401.06, 6.012408133905629, 33.22936070396758, 12.446771144138081], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getStaffHealthRecords", 236, 0, 0.0, 3718.3262711864404, 3, 92787, 190.0, 9532.400000000007, 25295.69999999998, 43386.37, 0.6737101733090873, 0.39672581494665987, 0.8306018126657513], "isController": false}, {"data": ["getCentreHolidaysOfYear", 132, 0, 0.0, 82164.58333333333, 635, 222895, 81182.0, 141300.3, 173326.84999999998, 217329.8799999998, 0.3516539724910688, 0.9434856725981633, 0.4515868885017143], "isController": false}, {"data": ["getPastChildrenData", 145, 0, 0.0, 228831.62068965525, 87296, 377281, 246765.0, 322714.0, 351609.69999999995, 376128.24, 0.36694833114429676, 0.7553604705859279, 0.8367425324432938], "isController": false}, {"data": ["suggestProgram", 129, 0, 0.0, 46441.41860465115, 128, 216242, 35210.0, 104875.0, 124187.0, 212797.39999999988, 0.3452123858992783, 0.27981082060195406, 0.42848138913865497], "isController": false}, {"data": ["findAllWithdrawalDraft", 113, 0, 0.0, 5954.230088495576, 14, 50789, 2430.0, 21430.200000000033, 32490.399999999994, 49866.259999999995, 0.3571507587083153, 0.23124116506212214, 1.1659716663690411], "isController": false}, {"data": ["findAllLevels", 124, 0, 0.0, 32136.387096774197, 13, 113240, 27600.0, 75519.0, 85859.75, 112227.25, 0.3366034539857649, 0.21432173046749875, 0.2820368784372913], "isController": false}, {"data": ["findAllClass", 112, 0, 0.0, 37877.401785714275, 727, 212218, 31655.0, 73924.90000000001, 110225.74999999994, 206905.5500000002, 0.2928464412620636, 29.1682491051188, 0.23679380211424672], "isController": false}, {"data": ["getStaffCheckInOutRecords", 133, 0, 0.0, 7391.3834586466155, 18, 53274, 3277.0, 21536.600000000042, 34857.299999999996, 52700.41999999999, 0.4192158457285688, 0.2485000179269934, 0.5485832356213692], "isController": false}, {"data": ["getChildrenToAssignToClass", 112, 0, 0.0, 16152.374999999995, 95, 86012, 7694.0, 48812.90000000001, 55446.64999999999, 83828.78000000007, 0.3198875823639121, 0.18212349659976637, 0.23460505308134572], "isController": false}, {"data": ["getCountStaffCheckInOut", 120, 0, 0.0, 6553.891666666668, 5, 73022, 2444.5, 20489.800000000007, 24006.0, 68749.33999999984, 0.384975682369397, 0.22745145296238786, 0.39249473866567425], "isController": false}, {"data": ["searchBroadcastingScope", 132, 0, 0.0, 158686.446969697, 85002, 338595, 149473.0, 234911.80000000002, 246063.65, 318813.4799999993, 0.31233658526197233, 0.607666206837805, 0.6121675064656039], "isController": false}, {"data": ["getTransferDrafts", 119, 0, 0.0, 5821.983193277313, 14, 68227, 2334.0, 17278.0, 23344.0, 61568.999999999905, 0.3738975520707326, 0.24025838795170124, 0.648843701200871], "isController": false}, {"data": ["registrationByID", 136, 0, 0.0, 127344.77941176474, 159, 296769, 125443.0, 192498.5, 216011.40000000008, 289158.0999999999, 0.3487528240003488, 0.46865664130515616, 1.5864166544859615], "isController": false}, {"data": ["leadByID", 138, 0, 0.0, 85135.23913043474, 3016, 317530, 79789.0, 155654.30000000002, 180261.39999999997, 285335.10999999876, 0.35830847243751024, 0.5193738510760939, 1.6179866958506322], "isController": false}, {"data": ["getRegEnrolmentForm", 129, 0, 0.0, 74547.24806201551, 227, 194743, 61960.0, 148864.0, 157536.0, 193542.99999999994, 0.34010466787065474, 0.6117949913655598, 1.4540803085329361], "isController": false}, {"data": ["findAllLeads", 126, 0, 0.0, 145513.9682539683, 80238, 317514, 132728.5, 220680.59999999998, 244004.3, 312869.7300000001, 0.32042397686844054, 0.541244434699628, 0.7253347445127394], "isController": false}, {"data": ["getEnrollmentPlansByYear", 131, 0, 0.0, 288179.5954198472, 108120, 409523, 299175.0, 404068.6, 406756.4, 408970.68, 0.3179218106496526, 0.3523840381712458, 0.5274894104431248], "isController": false}, {"data": ["getAvailableVacancy", 136, 0, 0.0, 36376.60294117646, 253, 165532, 31932.5, 78465.2, 88541.65000000002, 153080.38999999984, 0.3548430862999259, 0.19440133927173675, 0.46954334173476525], "isController": false}, {"data": ["getRegistrations", 138, 0, 0.0, 113183.65942028987, 22342, 258510, 113635.0, 183290.20000000004, 196673.29999999987, 254149.40999999983, 0.353479079417528, 0.6292343848055225, 1.091504735467015], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 2541, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
